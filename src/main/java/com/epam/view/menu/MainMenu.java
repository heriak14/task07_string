package com.epam.view.menu;

import com.epam.text.Text;
import com.epam.view.Command;

import java.util.*;

public class MainMenu extends Menu {
    private Text text;
    private ResourceBundle bundle;
    private Showable showable;

    public MainMenu() {
        text = new Text("text.txt");
        bundle = ResourceBundle.getBundle("MainMenu", LanguageMenu.getLocale());
        initMenu();
    }

    void initMenu() {
        Map<String, String> menu = super.getMenu();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("Q", bundle.getString("Q"));

        Map<String, Command> commandMenu = super.getCommandMenu();
        commandMenu.put("1", this::showSortMenu);
        commandMenu.put("2", this::showModifyMenu);
        commandMenu.put("3", this::showFindMenu);
        commandMenu.put("4", this::showLanguageMenu);
        commandMenu.put("Q", this::quit);
    }

    private void showSortMenu() {
        showable = new SortMenu(text, LanguageMenu.getLocale());
        showable.show();
    }

    private void showFindMenu() {
        showable = new FindMenu(text, LanguageMenu.getLocale());
        showable.show();
    }

    private void showModifyMenu() {
        showable = new ModifyMenu(text, LanguageMenu.getLocale());
        showable.show();
    }

    private void showLanguageMenu() {
        showable = new LanguageMenu();
        showable.show();
        bundle = ResourceBundle.getBundle("MainMenu", LanguageMenu.getLocale());
        initMenu();
        show();
    }

    private void quit() {
    }
}
