package com.epam.view.menu;

import com.epam.view.Command;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public abstract class Menu implements Showable{
    private static final Logger LOGGER = LogManager.getLogger();
    private static final Scanner SCANNER = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Command> commandMenu;

    public Menu() {
        menu = new LinkedHashMap<>();
        commandMenu = new LinkedHashMap<>();
    }
    Map<String, String> getMenu() {
        return menu;
    }

    Map<String, Command> getCommandMenu() {
        return commandMenu;
    }

    @Override
    public void show() {
        String key;
        do {
            printMenu();
            LOGGER.trace("Enter your choice: ");
            key = SCANNER.nextLine().toLowerCase();
            if (commandMenu.containsKey(key)) {
                commandMenu.get(key).execute();
            } else if (!key.equals("q")) {
                LOGGER.trace("Wrong input!\n");
            }
        } while (!key.equals("q"));
    }

    void printMenu() {
        LOGGER.trace("------------MENU------------\n");
        for (String key : menu.keySet()) {
            LOGGER.trace(key + " - " + menu.get(key) + "\n");
        }
    }

    abstract void initMenu();
}
