package com.epam.view.menu;

import com.epam.view.Command;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

public class LanguageMenu extends Menu {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final Scanner SCANNER = new Scanner(System.in);
    private static Locale locale = new Locale("en");

    public LanguageMenu() {
        initMenu();
    }

    @Override
    void initMenu() {
        Map<String, String> menu = super.getMenu();
        menu.put("1", "ENGLISH");
        menu.put("2", "DEUTSCH");
        menu.put("3", "\u65e5\u672c\u8a9e");
        menu.put("4", "\u0423\u041a\u0420\u0410\u0407\u041d\u0421\u042c\u041a\u0410");
        menu.put("Q", "Exit");

        Map<String, Command> commandMenu = super.getCommandMenu();
        commandMenu.put("1", this::switchEnglish);
        commandMenu.put("2", this::switchGerman);
        commandMenu.put("3", this::switchJapanese);
        commandMenu.put("4", this::switchUkrainian);
        commandMenu.put("Q", this::quit);
    }

    public static Locale getLocale() {
        return locale;
    }

    private void switchEnglish() {
        locale = new Locale("en");
    }

    private void switchGerman() {
        locale = new Locale("de");
    }

    private void switchJapanese() {
        locale = new Locale("ja");
    }

    private void switchUkrainian() {
        locale = new Locale("uk");
    }

    private void quit() {
    }

    @Override
    public void show() {
        printMenu();
        LOGGER.trace("Enter your choice: ");
        String key = SCANNER.nextLine().toLowerCase();
        if (getCommandMenu().containsKey(key)) {
            getCommandMenu().get(key).execute();
        }
    }
}
