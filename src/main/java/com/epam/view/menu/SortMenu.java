package com.epam.view.menu;

import com.epam.text.Text;
import com.epam.text.TextUtils;
import com.epam.view.Command;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class SortMenu extends Menu {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final Scanner SCANNER = new Scanner(System.in);
    private Text text;
    private ResourceBundle bundle;

    public SortMenu(Text text, Locale locale) {
        this.text = text;
        bundle = ResourceBundle.getBundle("SortMenu", locale);
        initMenu();
    }

    @Override
    void initMenu() {
        Map<String, String> menu = super.getMenu();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("6", bundle.getString("6"));
        menu.put("7", bundle.getString("7"));
        menu.put("Q", bundle.getString("Q"));

        Map<String, Command> commandMenu = super.getCommandMenu();
        commandMenu.put("1", this::sortSentencesByWordNum);
        commandMenu.put("2", this::sortWordsByFirstLetter);
        commandMenu.put("3", this::sortWordsByVowelPercentage);
        commandMenu.put("4", this::sortWordsByConsonant);
        commandMenu.put("5", this::sortWordsByNumberOfGivenLetter);
        commandMenu.put("6", this::sortWordByOccurrences);
        commandMenu.put("7", this::sortWordsByNumberOfGivenLetterDesc);
        commandMenu.put("Q", this::quit);
    }

    private void sortSentencesByWordNum() {
        TextUtils.printInAscWordCount(text);
    }

    private void sortWordsByFirstLetter() {
        TextUtils.sortWordsByFirstLetter(text);
    }

    private void sortWordsByVowelPercentage() {
        TextUtils.sortByVowelPercent(text);
    }

    private void sortWordsByConsonant() {
        TextUtils.sortVowelWords(text);
    }

    private void sortWordsByNumberOfGivenLetter() {
        LOGGER.info("Enter symbol to sort: ");
        TextUtils.sortWordsBySymOccur(text, SCANNER.nextLine().charAt(0));
    }

    private void sortWordByOccurrences() {
        TextUtils.sortWordByOccurrence(text);
    }

    private void sortWordsByNumberOfGivenLetterDesc() {
        LOGGER.info("Enter symbol to sort: ");
        TextUtils.sortWordsBySymOccurRev(text, SCANNER.nextLine().charAt(0));
    }

    private void quit() {
    }
}
