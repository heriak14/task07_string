package com.epam.view.menu;

public interface Showable {
    void show();
}
