package com.epam.view.menu;

import com.epam.text.Text;
import com.epam.text.TextUtils;
import com.epam.view.Command;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class ModifyMenu extends Menu {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final Scanner SCANNER = new Scanner(System.in);
    private Text text;
    private ResourceBundle bundle;

    public ModifyMenu(Text text, Locale locale) {
        this.text = text;
        bundle = ResourceBundle.getBundle("ModifyMenu", locale);
        initMenu();
    }

    @Override
    void initMenu() {
        Map<String, String> menu = super.getMenu();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("Q", bundle.getString("Q"));

        Map<String, Command> commandMenu = super.getCommandMenu();
        commandMenu.put("1", this::swapWords);
        commandMenu.put("2", this::deleteSubstring);
        commandMenu.put("3", this::deleteConsonantWords);
        commandMenu.put("4", this::deleteFirstLetterOccurrences);
        commandMenu.put("5", this::replaceWord);
        commandMenu.put("Q", this::quit);
    }

    private void swapWords() {
        TextUtils.swapWords(text);
        LOGGER.info("\n");
    }

    private void deleteSubstring() {
        LOGGER.info("Enter start and end symbols: ");
        String input = SCANNER.nextLine();
        if (input.matches("[a-zA-Z]\\s[a-zA-Z]")) {
            TextUtils.deleteSubstring(text, input.charAt(0), input.charAt(2));
            LOGGER.info("\n");
        } else {
            LOGGER.info("Wrong input!\n");
        }
    }

    private void deleteConsonantWords() {
        LOGGER.info("Enter length to delete: ");
        String input = SCANNER.nextLine();
        if (input.matches("[0-9]+")) {
            TextUtils.deleteWords(text, Integer.parseInt(input));
            LOGGER.info("\n");
        } else {
            LOGGER.error("Wrong length!\n");
        }
    }

    private void deleteFirstLetterOccurrences() {
        TextUtils.deleteFirstLetterOccurrence(text);
        LOGGER.info("\n");
    }

    private void replaceWord() {
        LOGGER.info("Enter number of sentence: ");
        int n = SCANNER.nextInt();
        LOGGER.info("Enter length of word: ");
        int l = SCANNER.nextInt();
        SCANNER.nextLine();
        LOGGER.info("Enter replacement string: ");
        String s = SCANNER.nextLine();
        TextUtils.replaceWords(text, n, l, s);
        LOGGER.info("\n");
    }

    private void quit() {
    }
}
