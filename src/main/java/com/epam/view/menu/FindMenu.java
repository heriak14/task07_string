package com.epam.view.menu;

import com.epam.text.Text;
import com.epam.text.TextUtils;
import com.epam.view.Command;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class FindMenu extends Menu {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final Scanner SCANNER = new Scanner(System.in);
    private Text text;
    private ResourceBundle bundle;

    public FindMenu(Text text, Locale locale) {
        this.text = text;
        bundle = ResourceBundle.getBundle("FindMenu", locale);
        initMenu();
    }

    @Override
    void initMenu() {
        Map<String, String> menu = super.getMenu();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("Q", bundle.getString("Q"));

        Map<String, Command> commandMenu = super.getCommandMenu();
        commandMenu.put("1", this::findNumOfSimilarSentences);
        commandMenu.put("2", this::findUniqueWord);
        commandMenu.put("3", this::findWordsInAskSentences);
        commandMenu.put("4", this::findLongestPalindrome);
        commandMenu.put("Q", this::quit);
    }

    private void findNumOfSimilarSentences() {
        LOGGER.info("The biggest number of sentences that contains the same words is: ");
        LOGGER.info(TextUtils.countSimilarSentences(text) + "\n");
    }

    private void findUniqueWord() {
        LOGGER.info("Word which is present only in first sentence: " + TextUtils.findUniqueWord(text) + "\n");
    }

    private void findWordsInAskSentences() {
        LOGGER.info("Enter length of the word to find: ");
        String input = SCANNER.nextLine();
        if (input.matches("[0-9]+")) {
            int length = Integer.parseInt(input);
            TextUtils.printAskingSentences(text, length);
            LOGGER.info("\n");
        } else {
            LOGGER.info("Wrong length!\n");
        }
    }

    private void findLongestPalindrome() {
        TextUtils.findLongestPalindrome(text);
        LOGGER.info("\n");
    }

    private void quit() {
    }
}
