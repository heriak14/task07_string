package com.epam.view;

public interface Command {
    void execute();
}
