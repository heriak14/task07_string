package com.epam.text.components;

import java.text.BreakIterator;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Sentence {

    private List<Word> words;
    private List<Symbol> punctuations;

    public Sentence(String sentence) {
        words = new ArrayList<>();
        punctuations = new ArrayList<>();
        BreakIterator iterator = BreakIterator.getWordInstance();
        iterator.setText(sentence);
        int start = iterator.first();
        int end = iterator.next();
        while (end != BreakIterator.DONE) {
            String word = sentence.substring(start, end);
            if (!word.matches("\\s+")) {
                if(word.matches("[.,!?;]")){
                    punctuations.add(new Symbol(word.charAt(0)));
                } else {
                    words.add(new Word(word));
                }
            }
            start = end;
            end = iterator.next();
        }

    }

    public int size() {
        return words.size();
    }

    public boolean contains(Word word) {
        return words.contains(word);
    }

    public Stream<Word> stream() {
        return words.stream();
    }

    public void remove(Word word) {
        words.remove(word);
    }

    public void replace(Word word, String replacement) {
        Collections.replaceAll(words, word, new Word(replacement));
    }

    public Word getBiggestWord() {
        return words.stream()
                .max(Comparator.comparingInt(Word::length))
                .orElse(new Word(""));
    }

    public Word getFirstVowelWord() {
        return words.stream()
                .filter(w -> w.getLetter(0).isVowel())
                .findAny()
                .orElse(new Word(""));
    }

    private int indexOf(Word word) {
        return words.indexOf(word);
    }

    public Sentence swap(Word word1, Word word2) {
        Collections.swap(words, indexOf(word1), indexOf(word2));
        return this;
    }

    public boolean isAsking() {
        return punctuations.contains(new Symbol('?'));
    }

    public int lastIndexOf(Symbol symbol) {
        return words.stream()
                .flatMap(Word::stream)
                .collect(Collectors.toList())
                .lastIndexOf(symbol);
    }

    public int indexOf(Symbol symbol) {
        return words.stream()
                .flatMap(Word::stream)
                .collect(Collectors.toList())
                .indexOf(symbol);
    }

    @Override
    public String toString() {
        return words.stream()
                .map(Word::toString)
                .collect(Collectors.joining(" "));
    }
}
