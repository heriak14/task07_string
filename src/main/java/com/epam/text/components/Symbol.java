package com.epam.text.components;

import java.util.Objects;

public class Symbol {
    private char symbol;

    public Symbol(char symbol) {
        this.symbol = symbol;
    }

    public Symbol(int code) {
        this.symbol = (char) code;
    }

    public char getSymbol() {
        return symbol;
    }

    public void setSymbol(char symbol) {
        this.symbol = symbol;
    }

    public boolean isVowel() {
        return toString().matches("[aoueiyAOUEIY]");
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (Objects.isNull(obj)) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        return this.symbol == ((Symbol) obj).symbol;
    }

    @Override
    public String toString() {
        return String.valueOf(symbol);
    }
}
