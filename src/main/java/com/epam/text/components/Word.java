package com.epam.text.components;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Word {
    private List<Symbol> symbols;

    public Word(String word) {
        symbols = word.chars()
                .mapToObj(Symbol::new)
                .collect(Collectors.toList());
    }

    public Symbol getLetter(int index) {
        return symbols.get(index);
    }

    public void remove(Symbol symbol) {
        symbols.remove(symbol);
    }

    public int length() {
        return symbols.size();
    }

    private boolean contains(Symbol s) {
        return symbols.contains(s);
    }

    public Stream<Symbol> stream() {
        return symbols.stream();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (Objects.isNull(o)) {
            return false;
        }
        if (this.getClass() != o.getClass()) {
            return false;
        }
        Word word = (Word) o;
        return this.stream().allMatch(word::contains);
    }

    @Override
    public String toString() {
        return symbols.stream()
                .map(Symbol::toString)
                .collect(Collectors.joining());
    }
}
