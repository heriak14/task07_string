package com.epam.text;

import com.epam.text.components.Sentence;
import com.epam.text.components.Symbol;
import com.epam.text.components.Word;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Comparator;
import java.util.function.BinaryOperator;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class TextUtils {
    private static final Logger LOGGER = LogManager.getLogger();

    public static long countSimilarSentences(Text text) {
        return text.stream()
                .flatMap(Sentence::stream)
                .distinct()
                .mapToLong(w -> text.stream().filter(s -> s.contains(w)).count())
                .max()
                .orElse(0);
    }

    public static void printInAscWordCount(Text text) {
        text.stream()
                .sorted(Comparator.comparingInt(Sentence::size))
                .forEach(s -> LOGGER.info(s + "\n"));
    }

    public static Word findUniqueWord(Text text) {
        return text.getSentence(0).stream()
                .filter(w -> text.stream()
                        .skip(1)
                        .noneMatch(s -> s.contains(w)))
                .findAny().orElse(new Word(""));
    }

    public static void printAskingSentences(Text text, int length) {
        text.stream()
                .filter(Sentence::isAsking)
                .flatMap(Sentence::stream)
                .filter(w -> w.length() == length)
                .distinct()
                .forEach(w -> LOGGER.info(w + " "));
    }

    public static void swapWords(Text text) {
        text.stream()
                .map(s -> s.swap(s.getBiggestWord(), s.getFirstVowelWord()))
                .forEach(s -> LOGGER.info(s + "\n"));
    }

    public static void sortWordsByFirstLetter(Text text) {
        Comparator<Word> comparator = (w1, w2) ->
                w1.getLetter(0).toString()
                        .compareToIgnoreCase(w2.getLetter(0).toString());
        BinaryOperator<Word> printer = (w1, w2) -> {
            if (comparator.compare(w1, w2) == 0) {
                LOGGER.info(w1 + " ");
            } else {
                LOGGER.info(w1 + "\n");
            }
            return w2;
        };
        text.stream()
                .flatMap(Sentence::stream)
                .sorted(comparator)
                .reduce(printer)
                .ifPresent(LOGGER::info);
    }

    public static void sortByVowelPercent(Text text) {
        Comparator<Word> comparator = (w1, w2) -> {
            long percent1 = w1.stream()
                    .filter(Symbol::isVowel)
                    .count() / w1.length() * 100;
            long percent2 = w2.stream()
                    .filter(Symbol::isVowel)
                    .count() / w2.length() * 100;
            return Long.compare(percent1, percent2);
        };
        text.stream()
                .flatMap(Sentence::stream)
                .sorted(comparator)
                .forEach(w -> LOGGER.info(w + " "));
    }

    public static void sortVowelWords(Text text) {
        Comparator<Word> comparator = (w1, w2) -> {
            char c1 = w1.stream()
                    .filter(s -> !s.isVowel())
                    .findFirst()
                    .orElse(new Symbol(0)).getSymbol();
            char c2 = w2.stream()
                    .filter(s -> !s.isVowel())
                    .findFirst()
                    .orElse(new Symbol(0)).getSymbol();
            return Character.compare(c1, c2);
        };
        text.stream()
                .flatMap(Sentence::stream)
                .filter(w -> w.getLetter(0).isVowel())
                .sorted(comparator)
                .forEach(w -> LOGGER.info(w + " "));
    }

    public static void sortWordsBySymOccur(Text text, char symbol) {
        text.stream()
                .flatMap(Sentence::stream)
                .sorted(getSymOccurComparator(symbol))
                .forEach(word -> LOGGER.info(word + " "));
    }

    public static void sortWordByOccurrence(Text text) {
        text.stream()
                .flatMap(Sentence::stream)
                .collect(Collectors.groupingBy(Word::toString, Collectors.counting()))
                .keySet()
                .stream()
                .sorted(Comparator.reverseOrder())
                .forEach(w -> LOGGER.info(w + "\n"));
    }

    public static void deleteSubstring(Text text, char start, char end) {
        Predicate<Sentence> containsLimits = s -> {
            int startIndex = s.indexOf(new Symbol(start));
            int endIndex = s.lastIndexOf(new Symbol(end));
            return startIndex != -1 && endIndex != -1
                    && startIndex < endIndex - 1;
        };
        text.stream()
                .filter(containsLimits)
                .forEach(s -> s.stream()
                        .flatMap(Word::stream)
                        .skip(s.indexOf(new Symbol(start)))
                        .limit(s.lastIndexOf(new Symbol(end)))
                        .forEach(symbol -> LOGGER.info(symbol + " ")));
    }

    public static void deleteWords(Text text, int length) {
        Predicate<Word> predicate = word -> word.length() == length
                && word.getLetter(0).toString().matches("[^aoueiyAOUEIY]");

        text.stream()
                .forEach(s -> s.stream()
                        .filter(predicate)
                        .forEach(s::remove));
    }

    public static void sortWordsBySymOccurRev(Text text, char symbol) {
        text.stream()
                .flatMap(Sentence::stream)
                .sorted(getSymOccurComparator(symbol).reversed())
                .forEach(word -> LOGGER.info(word + " "));
    }

    private static Comparator<Word> getSymOccurComparator(char symbol) {
        return (w1, w2) -> {
            long count1 = w1.stream().filter(s -> s.equals(new Symbol(symbol))).count();
            long count2 = w2.stream().filter(s -> s.equals(new Symbol(symbol))).count();
            return count1 == count2 ? w1.toString().compareTo(w2.toString()) : Long.compare(count1, count2);
        };
    }

    public static void findLongestPalindrome(Text text) {
        String textString = text.stream()
                .flatMap(Sentence::stream)
                .map(Word::toString)
                .collect(Collectors.joining(" "));
        String longest = textString.substring(0, 1);
        for (int i = 0; i < textString.length(); i = i + 1) {
            String tmp = checkForEquality(textString, i, i);
            if (tmp.length() > longest.length()) {
                longest = tmp;
            }
            tmp = checkForEquality(textString, i, i + 1);
            if (tmp.length() > longest.length()) {
                longest = tmp;
            }
        }
        LOGGER.info("Longest palindrome: " + longest);
    }

    private static String checkForEquality(String s, int begin, int end) {
        while (begin >= 0 && end <= s.length() - 1 && s.charAt(begin) == s.charAt(end)) {
            begin--;
            end++;
        }
        return s.substring(begin + 1, end);
    }

    public static void deleteFirstLetterOccurrence(Text text) {
        text.stream()
                .flatMap(Sentence::stream)
                .forEach(w -> w.stream()
                        .skip(1)
                        .filter(s -> s.equals(w.getLetter(0)))
                        .forEach(w::remove));
    }

    public static void replaceWords(Text text, int sentenceNum, int wordLength, String replacement) {
        Sentence sentence = text.getSentence(sentenceNum);
        sentence.stream()
                .filter(w -> w.length() == wordLength)
                .forEach(w -> sentence.replace(w, replacement));
    }
}
