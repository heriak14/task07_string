package com.epam.text;

import com.epam.text.components.Sentence;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.text.BreakIterator;
import java.util.*;
import java.util.stream.Stream;

public class Text {

    private static final Logger LOGGER = LogManager.getLogger();
    private static Scanner fileReader;
    private List<Sentence> sentences;

    public Text(String fileName) {
        try (InputStream input = getClass().getClassLoader().getResourceAsStream(fileName)) {
            Optional<InputStream> streamOptional = Optional.ofNullable(input);
            fileReader = new Scanner(streamOptional.orElse(System.in));
            readText();
        } catch (IOException e) {
            LOGGER.error(e.getMessage() + "\n");
        }
    }

    private void readText() {
        sentences = new ArrayList<>();
        StringBuilder lines = new StringBuilder();
        while (fileReader.hasNextLine()) {
            lines.append(fileReader.nextLine());
        }
        BreakIterator iterator = BreakIterator.getSentenceInstance();
        iterator.setText(lines.toString());
        int start = iterator.first();
        int end = iterator.next();
        while (end != BreakIterator.DONE) {
            sentences.add(new Sentence(lines.substring(start, end).replaceAll("\\s{2,}", " ")));
            start = end;
            end = iterator.next();
        }
    }

    public Sentence getSentence(int index) {
        return sentences.get(index);
    }

    public Stream<Sentence> stream() {
        return sentences.stream();
    }
}
