package com.epam.strings;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {

    public static String concatenate(Object... objects) {
        return Arrays.stream(objects)
                .map(Object::toString)
                .reduce((s1, s2) -> s1 + s2).orElse("");
    }

    public static boolean isSentence(String string) {
        Pattern pattern = Pattern.compile("[A-Z].*[.]");
        Matcher matcher = pattern.matcher(string);
        return matcher.matches();
    }

    public static String[] splitOnTheYou(String string) {
        return string.split("the|you");
    }

    public static String replaceVowels(String string) {
        return string.replaceAll("[aeiouyAEIOUY]", "_");
    }
}
